interface Config {
    APP_ENV: string | undefined,
    ENVIRONMENT: string | undefined;
    PORT: number | undefined;
  }
  
  export default Config;